#!/bin/bash

set -euo pipefail

until curl -s "http://localhost:5601/login | grep Loading Kibana" > /dev/null; do
	  echo "Waiting for kibana..."
	  sleep 1
done

chmod go-w /usr/share/metricbeat/metricbeat.yml

echo "Setting up dashboards..."
# Load the sample dashboards for the Beat.
# REF: https://www.elastic.co/guide/en/beats/metricbeat/master/metricbeat-sample-dashboards.html
sleep 15
