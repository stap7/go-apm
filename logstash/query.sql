SELECT * FROM aaafx_logging.log_data_260 WHERE date_time BETWEEN SUBDATE(UTC_TIMESTAMP(),INTERVAL 1 MINUTE) AND UTC_TIMESTAMP()
AND (message LIKE '%sending mail:%' 
OR (severity='WARN' AND message NOT LIKE '%processaccept%' AND message NOT LIKE '%Cannot determine buyInteres%' AND message NOT LIKE '%Cannot determine sellInteres%')
OR severity='ERROR') 
AND message` NOT LIKE '%dealer sync%' 
AND message` NOT LIKE '%will assume interest is zero%'
AND message` NOT LIKE '%Request is invalid%' 
AND message` NOT LIKE '%Not enough MarginLevel%'
AND message` NOT LIKE '%Cannot find symbol in mt4_symbol table%' 
AND message` NOT LIKE '%Exception getting symbol%'
AND message` NOT LIKE '%Invalid margin calculation method found!%'
